package icu.lifelesshub.trackit.fragments.project;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.MainActivity;
import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;
import icu.lifelesshub.trackit.entities.Project;

public class NewProjectFragment extends Fragment {

    private TrackitDatabaseHelper helper;

    public static final String TITLE = "New project";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        helper = MainActivity.getTrackitDatabaseHelper();

        View rootView = inflater.inflate(R.layout.fragment_project_new, container, false);
        return rootView;
    }
}
