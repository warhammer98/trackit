package icu.lifelesshub.trackit.fragments.project;

import android.content.Context;
import android.net.sip.SipSession;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.MainActivity;
import icu.lifelesshub.trackit.activity.project.DetailProjectActivity;
import icu.lifelesshub.trackit.adapters.ProjectsListAdapter;
import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;
import icu.lifelesshub.trackit.entities.Project;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectsListFragment extends ListFragment {


    public static final String TITLE = "Projects";

    public static interface ProjectsListener {
        void itemClicked(long id, Class t);
    }
    private ProjectsListener listener;

    private ProjectsListAdapter projectsAdapter;

    public ProjectsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (ProjectsListener) context;
    }
    @Override
    public void onResume() {
        super.onResume();
        projectsAdapter = new ProjectsListAdapter(getLayoutInflater().getContext(), R.layout.project_list_item, Project.getProjects());
        setListAdapter(projectsAdapter);
    }
    @Override
    public void onListItemClick(ListView listView, View itemView, int position, long id) {
        int projectId = (int)((ProjectsListAdapter)getListAdapter()).getProjectByIndex((int)id).getId();
        if (listener != null) {
            listener.itemClicked(projectId, DetailProjectActivity.class);
        }
    }

}
