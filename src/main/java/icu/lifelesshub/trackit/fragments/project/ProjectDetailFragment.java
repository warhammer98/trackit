package icu.lifelesshub.trackit.fragments.project;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.MainActivity;
import icu.lifelesshub.trackit.adapters.ProjectsListAdapter;
import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;
import icu.lifelesshub.trackit.entities.Project;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.fragments.task.TasksListFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectDetailFragment extends Fragment {


    // TODO Add summary time spent

    public static final String TITLE = "Edit project";
    public static final String TAG = "projectDetailFragment";

    private long projectId;

    public void setProject(int id) {
        this.projectId = id;
    }

    public ProjectDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("projectId", this.projectId);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {

            this.projectId = savedInstanceState.getLong("projectId");

        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }
    @Override
    public void onResume() {
        super.onResume();

        TrackitDatabaseHelper helper = MainActivity.getTrackitDatabaseHelper();

        View view = getView();
        if (view != null) {
            Project project = Project.getProjectById((int) projectId);
            EditText nameView = (EditText) getView().findViewById(R.id.project_title_input);
            nameView.setText(project.getName());
            EditText descriptionView = (EditText) getView().findViewById(R.id.project_description_input);
            descriptionView.setText(project.getDescription());
            loadTasksFragment();
            if (Task.getTasksByProjectId((int)projectId).size() == 0) {
                TextView tasksListTitle = getView().findViewById(R.id.tasks_list_title);
                tasksListTitle.setText("");
            }
            else {
                TextView tasksListTitle = getView().findViewById(R.id.tasks_list_title);
                tasksListTitle.setText(getResources().getString(R.string.tasks_list_title));
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_project_detail, container, false);
        return rootView;
    }

    private void loadTasksFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        TasksListFragment tasksListFragment = new TasksListFragment();
        tasksListFragment.setProject((int)projectId);
        transaction.add(R.id.project_tasks_fragment_container, tasksListFragment, TasksListFragment.TAG);
        transaction.commit();
    }
}
