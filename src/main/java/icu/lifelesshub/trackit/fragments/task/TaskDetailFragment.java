package icu.lifelesshub.trackit.fragments.task;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.MainActivity;
import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;
import icu.lifelesshub.trackit.entities.Project;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.entities.TaskTimeInterval;
import icu.lifelesshub.trackit.fragments.tasktimeinterval.TaskTimeIntervalsListFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskDetailFragment extends Fragment implements TaskTimeIntervalsListFragment.TaskTimeIntervalsListener {
    public static final String TITLE = "Edit task";
    public static final String TAG = "taskDetailFragment";

    private long taskId;
    private Handler handler;
    private Runnable timer;

    public void setTask(int id) {
        this.taskId = id;
    }

    public TaskDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("taskId", this.taskId);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {

            this.taskId = savedInstanceState.getLong("taskId");
            Log.d("TAG", "onCreate: " + this.taskId);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onResume() {
        super.onResume();

        TrackitDatabaseHelper helper = MainActivity.getTrackitDatabaseHelper();

        View view = getView();
        if (view != null) {
            Task task = Task.getTaskById((int) taskId);
            EditText nameView = (EditText) getView().findViewById(R.id.task_title_input);
            nameView.setText(task.getName());
            EditText descriptionView = (EditText) getView().findViewById(R.id.task_description_input);
            descriptionView.setText(task.getDescription());
            RadioGroup projectsRadioGroup = getView().findViewById(R.id.choose_project_radio_group);
            projectsRadioGroup.removeAllViews();
            ArrayList<Project> projects = Project.getProjects();
            for (Project project: projects) {
                RadioButton rb = new RadioButton(getContext());
                rb.setText(project.getName());
                rb.setId((int)project.getId());
                ColorStateList colorStateList = new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_checked}, new int[]{android.R.attr.state_enabled}},
                        new int[] {ContextCompat.getColor(getContext(), R.color.colorAccent),ContextCompat.getColor(getContext(), R.color.listItemTextColor)}
                );
                rb.setButtonTintList(colorStateList);
                rb.setTextColor(ContextCompat.getColor(getContext(), R.color.listItemTextColor));

                projectsRadioGroup.addView(rb);
                if ((int)project.getId() == task.getProjectId()) {
                    projectsRadioGroup.check(rb.getId());
                }
            }
            if (TaskTimeInterval.getTaskTimeIntervals((int)taskId).size() == 0) {
                TextView intervalsListTitle = getView().findViewById(R.id.intervals_title);
                intervalsListTitle.setText("");
            }
            else {
                TextView intervalsListTitle = getView().findViewById(R.id.intervals_title);
                intervalsListTitle.setText(getResources().getString(R.string.intervals_list_title));
            }
            if (task.getStartTime() > 0) {
                startTimer();
            }
            else {
                updateTimeString();
            }
            loadIntervalsFragment();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(timer);
        }
    }

    public void updateTimeString() {

        Task task = Task.getTaskById((int) taskId);
        TextView timeString = (TextView) getView().findViewById(R.id.time_string);
        timeString.setText(task.getTimeString());
    }
    private void startTimer() {
        final ImageButton taskToggleButton = getView().findViewById(R.id.task_toggle_button);
        taskToggleButton.setImageResource(R.drawable.baseline_pause_24);
        updateTimeString();
        handler = new Handler();
        timer = new Runnable() {
            @Override
            public void run() {
                updateTimeString();
                Task task = Task.getTaskById((int)taskId);
                if (task.getStartTime() > 0) {
                    handler.postDelayed(this, 100);
                }
                else {
                    taskToggleButton.setImageResource(R.drawable.baseline_play_arrow_24);
                }
            }
        };
        handler.post(timer);
    }
    private void stopTimer() {
        ImageButton taskToggleButton = getView().findViewById(R.id.task_toggle_button);
        taskToggleButton.setImageResource(R.drawable.baseline_play_arrow_24);
        updateTimeString();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_task_detail, container, false);

        ImageButton taskToggleButton = rootView.findViewById(R.id.task_toggle_button);
        taskToggleButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Task task = Task.getTaskById((int) taskId);
                if (task.getStartTime() > 0) {
                    task.stop();
                    stopTimer();
                    loadIntervalsFragment();
                }
                else {
                    task.start();
                    startTimer();
                    loadIntervalsFragment();
                }
            }
        });
        return rootView;
    }
    public void loadIntervalsFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        TaskTimeIntervalsListFragment intervalsListFragment = new TaskTimeIntervalsListFragment();
        intervalsListFragment.setTask((int)taskId);

        transaction.replace(R.id.task_intervals_fragment_container, intervalsListFragment, TaskTimeIntervalsListFragment.TAG);
        transaction.commit();
    }

    @Override
    public void reloadIntervalsList() {
        loadIntervalsFragment();
        updateTimeString();
    }
}
