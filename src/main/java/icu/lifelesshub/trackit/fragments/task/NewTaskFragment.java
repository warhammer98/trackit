package icu.lifelesshub.trackit.fragments.task;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.MainActivity;
import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;
import icu.lifelesshub.trackit.entities.Project;

public class NewTaskFragment extends Fragment {
    private TrackitDatabaseHelper helper;

    public static final String TITLE = "New task";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        helper = MainActivity.getTrackitDatabaseHelper();

        View rootView = inflater.inflate(R.layout.fragment_task_new, container, false);
        RadioGroup projectsRadioGroup = rootView.findViewById(R.id.choose_project_radio_group);
        ArrayList<Project> projects = Project.getProjects();
        for (Project project: projects) {
            RadioButton rb = new RadioButton(getContext());
            rb.setText(project.getName());
            rb.setId((int)project.getId());
            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{new int[]{android.R.attr.state_checked}, new int[]{android.R.attr.state_enabled}},
                    new int[] {ContextCompat.getColor(getContext(), R.color.colorAccent),ContextCompat.getColor(getContext(), R.color.listItemTextColor)}
            );
            rb.setButtonTintList(colorStateList);
            rb.setTextColor(ContextCompat.getColor(getContext(), R.color.listItemTextColor));
            projectsRadioGroup.addView(rb);
        }

        return rootView;
    }

}
