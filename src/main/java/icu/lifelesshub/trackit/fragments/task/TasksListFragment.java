package icu.lifelesshub.trackit.fragments.task;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.task.DetailTaskActivity;
import icu.lifelesshub.trackit.adapters.TasksListAdapter;
import icu.lifelesshub.trackit.entities.Task;

/**
 * A simple {@link Fragment} subclass.
 */
public class TasksListFragment extends ListFragment {

    public static final String TITLE = "Tasks";
    public static final String TAG = "tasksListFragment";
    private Integer projectId;

    public void setProject(int id) {
        this.projectId = id;
    }

    public static interface TasksListener {
        void itemClicked(long id, Class t);
    }
    private TasksListener listener;

    private TasksListAdapter tasksAdapter;

    public TasksListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (TasksListener) context;
    }
    @Override
    public void onResume() {
        super.onResume();
        if (projectId != null) {
            tasksAdapter = new TasksListAdapter(getLayoutInflater().getContext(), R.layout.task_list_item, Task.getTasksByProjectId(projectId));
            tasksAdapter.setScrollable(false);
        }
        else {
            tasksAdapter = new TasksListAdapter(getLayoutInflater().getContext(), R.layout.task_list_item, Task.getTasks());
        }
        setListAdapter(tasksAdapter);
    }
    @Override
    public void onListItemClick(ListView listView, View itemView, int position, long id) {
        int taskId = (int)((TasksListAdapter)getListAdapter()).getTaskByIndex((int)id).getId();

        if (listener != null) {
            listener.itemClicked(taskId, DetailTaskActivity.class);
        }
    }

}
