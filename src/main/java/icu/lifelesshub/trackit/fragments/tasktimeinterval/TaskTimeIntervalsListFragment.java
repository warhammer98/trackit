package icu.lifelesshub.trackit.fragments.tasktimeinterval;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.task.DetailTaskActivity;
import icu.lifelesshub.trackit.adapters.TaskTimeIntervalsListAdapter;
import icu.lifelesshub.trackit.adapters.TasksListAdapter;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.entities.TaskTimeInterval;
import icu.lifelesshub.trackit.fragments.task.TaskDetailFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskTimeIntervalsListFragment extends ListFragment {

    public static final String TITLE = "TaskTimeIntervals";
    public static final String TAG = "taskTimeIntervalsListFragment";
    private Integer taskId;

    public void setTask(int id) {
        this.taskId = id;
    }

    public static interface TasksListener {
        void itemClicked(long id, Class t);
    }

    private TaskTimeIntervalsListAdapter intervalsAdapter;

    public TaskTimeIntervalsListFragment() {
        // Required empty public constructor
    }

    public interface TaskTimeIntervalsListener {
        public void reloadIntervalsList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //this.listener = (TasksListener) context;
    }
    @Override
    public void onResume() {
        super.onResume();
        intervalsAdapter = new TaskTimeIntervalsListAdapter(getLayoutInflater().getContext(), R.layout.task_time_interval_list_item, TaskTimeInterval.getTaskTimeIntervals(taskId));
        intervalsAdapter.setTaskId(taskId);
        setListAdapter(intervalsAdapter);
    }
    /*@Override
    public void onListItemClick(ListView listView, View itemView, int position, long id) {
        int taskId = (int)((TasksListAdapter)getListAdapter()).getTaskByIndex((int)id).getId();

        if (listener != null) {
            listener.itemClicked(taskId, DetailTaskActivity.class);
        }
    }*/
    @Override
    public void onListItemClick(ListView listView, View itemView, int position, final long id) {
        PopupMenu popup = new PopupMenu(getContext(), itemView);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.task_time_interval_popup_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.delete_interval) {
                    TaskTimeInterval interval = ((TaskTimeIntervalsListAdapter)getListAdapter()).getIntervalByIndex((int)id);
                    interval.delete();

                    TaskDetailFragment parentFragment = (TaskDetailFragment) getFragmentManager().findFragmentById(R.id.task_detail_fragment);
                    parentFragment.loadIntervalsFragment();
                    parentFragment.updateTimeString();
                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }


}
