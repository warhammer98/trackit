package icu.lifelesshub.trackit.entities;

import java.util.ArrayList;

import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;

import static icu.lifelesshub.trackit.other.FHelper.getUnixTimestamp;

public class Task {
    private long id;
    private String name;
    private String description;
    private int projectId;
    private int startTime;
    private int time;


    public static ArrayList<Task> getTasks() {
        return TrackitDatabaseHelper.getTasks();
    }
    public static ArrayList<Task> getTasksByProjectId(int projectId) {
        return TrackitDatabaseHelper.getTasksByProjectId(projectId);
    }
    public static Task getTaskById(int id) {
        return TrackitDatabaseHelper.getTaskById(id);
    }

    public Task(long id, String name, String description, int projectId, int time) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.time = time;
    }
    public Task(long id, String name, String description, int projectId, int time, int startTime) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.time = time;
        this.startTime = startTime;
    }
    public static void createTask (String name, String description, int project_id) {
        TrackitDatabaseHelper.insertTask(name, description, project_id);
    }
    public void update(String name, String description, int project_id) {
        TrackitDatabaseHelper.updateTask((int)this.id, name, description, project_id);
    }

    public void start() {
        ArrayList<Task> tasks = getTasks();
        for (Task task : tasks) {
            if (task.startTime > 0) {
                task.stop();
            }
        }
        this.startTime = TrackitDatabaseHelper.startTask((int)this.id);
    }
    public void stop() {
        TrackitDatabaseHelper.stopTask((int)this.id);
        this.startTime = 0;

    }
    public static void removeTask (int id) {
        TrackitDatabaseHelper.deleteTask(id);
    }

    public String getTimeString() {
        int seconds;
        if (this.getStartTime() > 0) {
            seconds = getUnixTimestamp() - this.getStartTime() + this.time;
        }
        else {
            seconds = this.time;
        }
        int hours = (int) seconds / 3600;
        int buf = (int) seconds - hours * 3600;
        int mins = buf / 60;
        buf = buf - mins * 60;
        int secs = buf;
        return String.format("%02d:%02d:%02d", hours,mins,secs);
    }
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getProjectId() {
        return projectId;
    }

    public int getStartTime() { return startTime; }
}
