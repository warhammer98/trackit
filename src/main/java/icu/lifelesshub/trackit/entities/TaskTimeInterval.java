package icu.lifelesshub.trackit.entities;

import java.util.ArrayList;

import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;

import static icu.lifelesshub.trackit.other.FHelper.getUnixTimestamp;

public class TaskTimeInterval {
    private long id;
    private int taskId;
    private int startTime;
    private int stopTime;

    public TaskTimeInterval(long id, int taskId, int startTime, int stopTime) {
        this.id = id;
        this.taskId = taskId;
        this.startTime = startTime;
        this.stopTime = stopTime;
    }

    public static ArrayList<TaskTimeInterval> getTaskTimeIntervals(int taskId) {
        return TrackitDatabaseHelper.getTaskTimeIntervals(taskId);
    }

    public void delete() {
        TrackitDatabaseHelper.deleteTaskTimeInterval((int)this.id);
    }
    public String getTimeString() {
        int seconds;
        if (this.stopTime > 0) {
            seconds = this.stopTime - this.startTime;
        }
        else {
            seconds = getUnixTimestamp() - this.startTime;
        }
        int hours = (int) seconds / 3600;
        int buf = (int) seconds - hours * 3600;
        int mins = buf / 60;
        buf = buf - mins * 60;
        int secs = buf;
        return String.format("%02d:%02d:%02d", hours,mins,secs);
    }

    public long getId() {
        return id;
    }

    public int getTaskId() { return taskId; }

    public int getStopTime() { return stopTime; }

    public int getStartTime() { return startTime; }
}
