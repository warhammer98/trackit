package icu.lifelesshub.trackit.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;

import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;

public class Project {
    private long id;
    private String name;
    private String description;
    private int tasksCount;


    public static ArrayList<Project> getProjects() {
        return TrackitDatabaseHelper.getProjects();
    }


    public static Project getProjectById(int id) {
        return TrackitDatabaseHelper.getProjectById(id);
    }

    public Project(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.tasksCount = TrackitDatabaseHelper.getProjectTasksCount((int)this.id);
    }


    public static void createProject (String name, String description) {
        TrackitDatabaseHelper.insertProject(name, description);
    }

    public static void editProject (int id, String name, String description) {
        TrackitDatabaseHelper.updateProject(id, name, description);
    }
    public static void removeProject (int id) {
        TrackitDatabaseHelper.deleteProject(id);
    }


    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getDescription() { return description; }
    public int getTasksCount() { return tasksCount; }
}
