package icu.lifelesshub.trackit.other;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;

import androidx.core.content.ContextCompat;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.MainActivity;

public class UIHelper {
    public static GradientDrawable getRedBorder(Context context) {
        GradientDrawable border = (GradientDrawable) ContextCompat.getDrawable(context, R.drawable.input_background);
        //border.setColor(0xffff0000);
        border.setStroke(3, 0xffff0000);
        return border;
    }
}
