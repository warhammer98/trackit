package icu.lifelesshub.trackit.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import icu.lifelesshub.trackit.activity.MainActivity;
import icu.lifelesshub.trackit.entities.Project;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.entities.TaskTimeInterval;

import static icu.lifelesshub.trackit.other.FHelper.getUnixTimestamp;

public class TrackitDatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "trackit";
    private static final int DB_VERSION = 13;

    private static TrackitDatabaseHelper trackitDatabaseHelper;


    public static TrackitDatabaseHelper getInstance(Context context) {
        if (trackitDatabaseHelper == null) {
            trackitDatabaseHelper = new TrackitDatabaseHelper(context);
        }
        return trackitDatabaseHelper;
    }
    private TrackitDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE PROJECT (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME TEXT," +
                "DESCRIPTION TEXT," +
                "LAST_MODIFY INTEGER DEFAULT CURRENT_TIMESTAMP" +
                ");");
        db.execSQL("CREATE TABLE TASK (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME TEXT," +
                "DESCRIPTION TEXT," +
                "PROJECT_ID INTEGER," +
                "LAST_MODIFY INTEGER DEFAULT CURRENT_TIMESTAMP" +
                ");"
        );
        db.execSQL("CREATE TABLE TASK_TIME_INTERVAL (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "TASK_ID INTEGER," +
                "START_TIME INTEGER," +
                "STOP_TIME INTEGER" +
                ");"
        );
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion < 13 ) {

            db.execSQL("DROP TABLE PROJECT;");
            db.execSQL("DROP TABLE TASK;");
            db.execSQL("DROP TABLE TASK_TIME_INTERVAL;");
            db.execSQL("CREATE TABLE PROJECT (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "NAME TEXT," +
                    "DESCRIPTION TEXT," +
                    "LAST_MODIFY INTEGER DEFAULT CURRENT_TIMESTAMP" +
                    ");");
            db.execSQL("CREATE TABLE TASK (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "NAME TEXT," +
                    "DESCRIPTION TEXT," +
                    "PROJECT_ID INTEGER," +
                    "LAST_MODIFY INTEGER DEFAULT CURRENT_TIMESTAMP" +
                    ");"
            );
            db.execSQL("CREATE TABLE TASK_TIME_INTERVAL (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "TASK_ID INTEGER," +
                    "START_TIME INTEGER," +
                    "STOP_TIME INTEGER" +
                    ");"
            );
        }
    }



    // TODO Correct sorting of projects and tasks



    public static void insertProject(String name, String description) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        ContentValues projectValues = new ContentValues();
        projectValues.put("NAME", name);
        projectValues.put("DESCRIPTION", description);
        db.insert("PROJECT", null, projectValues);
        db.close();
    }
    public static void updateProject(int id, String name, String description) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        ContentValues projectValues = new ContentValues();
        projectValues.put("NAME", name);
        projectValues.put("DESCRIPTION", description);
        db.update("PROJECT", projectValues, "_id = ?", new String[] {Integer.toString(id)});
        db.close();
        updateLastModify(id, "PROJECT");
    }

    public static void deleteProject(int id) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        db.delete("PROJECT",
                "_id = ?",
                new String[] {Integer.toString(id)});
        db.close();
    }
    public static ArrayList<Project> getProjects() {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor cursor = db.query("PROJECT", new String[] {"_id", "NAME", "DESCRIPTION"}, null, null, null, null, "LAST_MODIFY DESC");
        ArrayList<Project> projects = new ArrayList<>();
        while (cursor.moveToNext()) {
            projects.add(new Project(cursor.getInt(0),cursor.getString(1), cursor.getString(2)));
        }
        cursor.close();
        db.close();
        return projects;
    }
    public static Project getProjectById(int id) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Log.d("TAG", "getProject1: " + id);
        Cursor cursor = db.query("PROJECT", new String[] {"_id", "NAME", "DESCRIPTION"}, "_id = ?", new String[] {Integer.toString(id)}, null, null, "_id ASC");
        Project project = null;
        if (cursor.moveToFirst()) {
            project = new Project(cursor.getInt(0),cursor.getString(1), cursor.getString(2));
        }
        cursor.close();
        db.close();
        return project;
    }

    public static int getProjectTasksCount(int projectId) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor cursor = db.query("TASK", new String[] {"_id"}, "PROJECT_ID = ?", new String[] {Integer.toString(projectId)}, null, null, "LAST_MODIFY DESC");
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public static void insertTask(String name, String description, int projectId) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        ContentValues taskValues = new ContentValues();
        taskValues.put("NAME", name);
        taskValues.put("DESCRIPTION", description);
        taskValues.put("PROJECT_ID", projectId);
        taskValues.put("LAST_MODIFY", getUnixTimestamp());
        db.insert("TASK", null, taskValues);
        db.close();
        updateLastModify(projectId, "PROJECT");

    }
    public static ArrayList<Task> getTasks() {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor cursor = db.query("TASK", new String[] {"_id", "NAME", "DESCRIPTION","PROJECT_ID","LAST_MODIFY"}, null, null, null, null, "LAST_MODIFY DESC");
        ArrayList<Task> tasks = new ArrayList<>();
        while (cursor.moveToNext()) {
            int taskId = cursor.getInt(0);
            int time = queryTime(taskId);
            int startTime = queryStartTime(taskId);
            tasks.add(new Task(cursor.getInt(0),cursor.getString(1), cursor.getString(2),cursor.getInt(3),time, startTime));
        }
        cursor.close();
        db.close();a
        return tasks;
    }

    public static ArrayList<Task> getTasksByProjectId(int projectId) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor cursor = db.query("TASK", new String[] {"_id", "NAME", "DESCRIPTION","PROJECT_ID","LAST_MODIFY"}, "PROJECT_ID = ?", new String[] {Integer.toString(projectId)}, null, null, "LAST_MODIFY DESC");
        ArrayList<Task> tasks = new ArrayList<>();
        while (cursor.moveToNext()) {
            int taskId = cursor.getInt(0);
            int time = queryTime(taskId);
            int startTime = queryStartTime(taskId);
            tasks.add(new Task(taskId, cursor.getString(1), cursor.getString(2),cursor.getInt(3),time, startTime));
        }
        cursor.close();
        db.close();
        return tasks;
    }
    public static Task getTaskById(int id) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor cursor = db.query("TASK", new String[] {"_id", "NAME", "DESCRIPTION","PROJECT_ID","LAST_MODIFY"}, "_id = ?", new String[] {Integer.toString(id)}, null, null, "_id ASC");
        Task task = null;
        if (cursor.moveToFirst()) {
            int taskId = cursor.getInt(0);
            int time = queryTime(taskId);
            int startTime = queryStartTime(taskId);
            task = new Task(cursor.getInt(0),cursor.getString(1), cursor.getString(2), cursor.getInt(3), time, startTime);
        }
        cursor.close();
        db.close();
        return task;
    }
    private static int queryTime(int taskId) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor taskIntervalCursor = db.rawQuery("SELECT SUM(STOP_TIME - START_TIME) FROM TASK_TIME_INTERVAL WHERE STOP_TIME != 0 AND TASK_ID = ?", new String[]{Integer.toString(taskId)});
        int time = 0;
        if (taskIntervalCursor.moveToNext()) {
            time = taskIntervalCursor.getInt(0);
        }
        return time;
    }
    private static int queryStartTime(int taskId) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor activeTaskIntervalCursor = db.rawQuery("SELECT START_TIME FROM TASK_TIME_INTERVAL WHERE STOP_TIME = 0 AND TASK_ID = ?", new String[]{Integer.toString(taskId)});
        int startTime = 0;
        if (activeTaskIntervalCursor.moveToNext()) {
            startTime = activeTaskIntervalCursor.getInt(0);
        }
        return startTime;
    }
    public static void updateTask(int id, String name, String description, int projectId) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        ContentValues taskValues = new ContentValues();
        taskValues.put("NAME", name);
        taskValues.put("DESCRIPTION", description);
        taskValues.put("PROJECT_ID", projectId);
        taskValues.put("LAST_MODIFY", getUnixTimestamp());
        db.update("TASK", taskValues, "_id = ?", new String[] {Integer.toString(id)});
        db.close();
    }
    public static void deleteTask(int id) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        db.delete("TASK",
                "_id = ?",
                new String[] {Integer.toString(id)});
        db.delete("TASK_TIME_INTERVAL",
                "TASK_ID = ?",
                new String[] {Integer.toString(id)});
        db.close();
    }
    private static void updateLastModify(int id, String table) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("LAST_MODIFY", getUnixTimestamp());
        db.update(table, values, "_id = ?", new String[] {Integer.toString(id)});
        db.close();
    }
    public static int startTask(int id) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        ContentValues values = new ContentValues();
        int time = getUnixTimestamp();
        values.put("TASK_ID", id);
        values.put("START_TIME", time);
        Log.d("TAG", "startTask: " + time);
        values.put("STOP_TIME", 0);
        db.insert("TASK_TIME_INTERVAL", null, values);
        db.close();
        updateLastModify(id, "TASK");
        return time;
    }

    public static void stopTask(int id) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor cursor = db.query("TASK_TIME_INTERVAL", new String[] {"_id"}, "TASK_ID = ? AND STOP_TIME = ?", new String[] {Integer.toString(id), "0"}, null, null, "_id ASC");
        if (cursor.moveToFirst()) {
            int taskIntervalId = cursor.getInt(0);
            db.close();
            db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
            ContentValues taskValues = new ContentValues();
            int time = getUnixTimestamp();
            Log.d("TAG", "stopTask: " + time);
            taskValues.put("STOP_TIME", time);
            db.update("TASK_TIME_INTERVAL", taskValues, "_id = ?", new String[] {Integer.toString(taskIntervalId)});
            db.close();
            updateLastModify(id, "TASK");
        }

    }

    public static ArrayList<TaskTimeInterval> getTaskTimeIntervals(int taskId) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getReadableDatabase();
        Cursor cursor = db.query("TASK_TIME_INTERVAL", new String[] {"_id", "TASK_ID", "START_TIME","STOP_TIME"}, "TASK_ID = ?", new String[] {Integer.toString(taskId)}, null, null, "_id DESC");
        ArrayList<TaskTimeInterval> intervals = new ArrayList<>();
        while (cursor.moveToNext()) {
            intervals.add(new TaskTimeInterval(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),cursor.getInt(3)));
        }
        cursor.close();
        db.close();
        return intervals;
    }
    public static void deleteTaskTimeInterval(int id) {
        SQLiteDatabase db = MainActivity.getTrackitDatabaseHelper().getWritableDatabase();
        db.delete("TASK_TIME_INTERVAL",
                "_id = ?",
                new String[] {Integer.toString(id)});
        db.close();
    }
}
