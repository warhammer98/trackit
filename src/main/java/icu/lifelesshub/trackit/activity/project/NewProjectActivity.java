package icu.lifelesshub.trackit.activity.project;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.entities.Project;
import icu.lifelesshub.trackit.fragments.project.NewProjectFragment;
import icu.lifelesshub.trackit.fragments.project.ProjectsListFragment;

public class NewProjectActivity extends AppCompatActivity {
    private ActionBar actionBar;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(NewProjectFragment.TITLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_project_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.save_project) {
            String name = ((EditText) findViewById(R.id.project_title_input)).getText().toString();
            String description = ((EditText) findViewById(R.id.project_description_input)).getText().toString();
            if (!name.isEmpty()) {
                Project.createProject(name, description);
                finish();
            }
            else {
                Toast toast = Toast.makeText(this, getResources().getString(R.string.empty_project_name_message), Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        return true;
    }
}
