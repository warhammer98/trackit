package icu.lifelesshub.trackit.activity.project;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.entities.Project;
import icu.lifelesshub.trackit.fragments.project.DeleteProjectDialog;
import icu.lifelesshub.trackit.fragments.project.ProjectDetailFragment;
import icu.lifelesshub.trackit.fragments.project.ProjectsListFragment;
import icu.lifelesshub.trackit.fragments.task.TasksListFragment;

public class DetailProjectActivity extends AppCompatActivity implements DeleteProjectDialog.NoticeDialogListener, TasksListFragment.TasksListener {
    private ActionBar actionBar;
    private Toolbar toolbar;
    private View detailFragment;
    private int projectId;

    public static final String EXTRA_FIELD_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_project);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        this.detailFragment = findViewById(R.id.project_detail_fragment);
        actionBar.setTitle(ProjectDetailFragment.TITLE);

        Intent intent = getIntent();
        projectId = (int) intent.getExtras().get(EXTRA_FIELD_ID);

        ProjectDetailFragment projectDetailFragment = (ProjectDetailFragment) getSupportFragmentManager().findFragmentById(R.id.project_detail_fragment);
        projectDetailFragment.setProject(projectId);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_project_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.delete_project) {
            DeleteProjectDialog deleteProjectDialog = new DeleteProjectDialog();
            deleteProjectDialog.show(getSupportFragmentManager(), "DELETE_PROJECT");
        }
        if(item.getItemId() == R.id.save_project) {
            String name = ((EditText) findViewById(R.id.project_title_input)).getText().toString();
            String description = ((EditText) findViewById(R.id.project_description_input)).getText().toString();

            if (!name.isEmpty()) {
                Project.editProject((int)projectId, name, description);
                Toast toast = Toast.makeText(this, getResources().getString(R.string.successful_update_message), Toast.LENGTH_SHORT);
                toast.show();
            }
            else {
                Toast toast = Toast.makeText(this, getResources().getString(R.string.empty_project_name_message), Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        return true;
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        Project.removeProject(projectId);
        this.finish();
    }

    @Override
    public void itemClicked(long id, Class T) {
        Intent intent = new Intent(this, T);
        intent.putExtra(DetailProjectActivity.EXTRA_FIELD_ID, (int) id);
        startActivity(intent);
    }
}
