package icu.lifelesshub.trackit.activity.task;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.entities.Project;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.fragments.project.DeleteProjectDialog;
import icu.lifelesshub.trackit.fragments.project.ProjectDetailFragment;
import icu.lifelesshub.trackit.fragments.task.DeleteTaskDialog;
import icu.lifelesshub.trackit.fragments.task.TaskDetailFragment;

public class DetailTaskActivity extends AppCompatActivity implements DeleteTaskDialog.NoticeDialogListener {
    private ActionBar actionBar;
    private Toolbar toolbar;
    private int taskId;

    public static final String EXTRA_FIELD_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_task);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(TaskDetailFragment.TITLE);

        Intent intent = getIntent();
        taskId = (int) intent.getExtras().get(EXTRA_FIELD_ID);

        TaskDetailFragment taskDetailFragment = (TaskDetailFragment) getSupportFragmentManager().findFragmentById(R.id.task_detail_fragment);
        taskDetailFragment.setTask(taskId);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_task_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.delete_task) {
            DeleteTaskDialog deleteTaskDialog = new DeleteTaskDialog();
            deleteTaskDialog.show(getSupportFragmentManager(), "DELETE_PROJECT");
        }
        if(item.getItemId() == R.id.save_task) {
            EditText nameInput = (EditText) findViewById(R.id.task_title_input);
            String name = nameInput.getText().toString();

            EditText descriptionInput = (EditText) findViewById(R.id.task_description_input);

            String description = descriptionInput.getText().toString();

            RadioGroup rg = findViewById(R.id.choose_project_radio_group);
            int project_id = rg.getCheckedRadioButtonId();

            boolean error = false;

            ArrayList<String> errorStrings = new ArrayList<>();
            if (name.isEmpty()) {
                errorStrings.add(getResources().getString(R.string.empty_task_name_message));
                error = true;
            }
            if (project_id == -1) {
                errorStrings.add(getResources().getString(R.string.empty_task_project_message));
                error = true;
            }
            if (error) {
                Toast toast = Toast.makeText(this, String.join("\n", errorStrings), Toast.LENGTH_SHORT);
                toast.show();
                return false;
            }


            Task task = Task.getTaskById((int)taskId);
            task.update(name, description, project_id);
            Toast toast = Toast.makeText(this, getResources().getString(R.string.successful_update_message), Toast.LENGTH_SHORT);
            toast.show();
        }
        return true;
    }
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        Task.removeTask(taskId);
        this.finish();
    }
}
