package icu.lifelesshub.trackit.activity.task;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.fragments.task.DeleteTaskDialog;
import icu.lifelesshub.trackit.fragments.task.NewTaskFragment;

public class NewTaskActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(NewTaskFragment.TITLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_task_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.save_task) {
            EditText nameInput = (EditText) findViewById(R.id.task_title_input);
            String name = nameInput.getText().toString();

            EditText descriptionInput = (EditText) findViewById(R.id.task_description_input);

            String description = descriptionInput.getText().toString();

            RadioGroup rg = findViewById(R.id.choose_project_radio_group);
            int project_id = rg.getCheckedRadioButtonId();

            boolean error = false;

            ArrayList<String> errorStrings = new ArrayList<>();
            if (name.isEmpty()) {
                errorStrings.add(getResources().getString(R.string.empty_task_name_message));
                error = true;
            }
            if (project_id == -1) {
                errorStrings.add(getResources().getString(R.string.empty_task_project_message));
                error = true;
            }
            if (error) {
                Toast toast = Toast.makeText(this, String.join("\n", errorStrings), Toast.LENGTH_SHORT);
                toast.show();
                return false;
            }


            Task.createTask(name, description, project_id);
            finish();
            Toast toast = Toast.makeText(this, getResources().getString(R.string.successful_create_message), Toast.LENGTH_SHORT);
            toast.show();
        }
        return true;
    }
}
