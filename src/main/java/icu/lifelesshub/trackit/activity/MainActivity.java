package icu.lifelesshub.trackit.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
//import android.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.project.DetailProjectActivity;
import icu.lifelesshub.trackit.activity.project.NewProjectActivity;
import icu.lifelesshub.trackit.activity.task.NewTaskActivity;
import icu.lifelesshub.trackit.db.TrackitDatabaseHelper;
import icu.lifelesshub.trackit.fragments.project.ProjectDetailFragment;
import icu.lifelesshub.trackit.fragments.project.ProjectsListFragment;
import icu.lifelesshub.trackit.fragments.task.TasksListFragment;

public class MainActivity extends AppCompatActivity implements ProjectsListFragment.ProjectsListener, TasksListFragment.TasksListener {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private View detailFragmentContainer;
    private static TrackitDatabaseHelper trackitDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trackitDatabaseHelper = TrackitDatabaseHelper.getInstance(this);
        setContentView(R.layout.activity_main);


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        this.detailFragmentContainer = findViewById(R.id.project_detail_fragment_container);
        actionBar.setTitle(ProjectsListFragment.TITLE);
        trackitDatabaseHelper = TrackitDatabaseHelper.getInstance(this);
        // at first run of activity
        if (this.detailFragmentContainer != null && savedInstanceState == null) {
            replaceDetailFragment(0);
        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_projects:
                        findViewById(R.id.layoutProjects).setVisibility(View.VISIBLE);
                        findViewById(R.id.layoutTasks).setVisibility(View.GONE);
                        //findViewById(R.id.layoutStats).setVisibility(View.GONE);
                        toolbar.getMenu().clear();
                        toolbar.inflateMenu(R.menu.projects_menu);
                        actionBar.setTitle(ProjectsListFragment.TITLE);
                        break;
                    case R.id.action_tasks:
                        findViewById(R.id.layoutProjects).setVisibility(View.GONE);
                        findViewById(R.id.layoutTasks).setVisibility(View.VISIBLE);
                        //findViewById(R.id.layoutStats).setVisibility(View.GONE);
                        toolbar.getMenu().clear();
                        toolbar.inflateMenu(R.menu.tasks_menu);
                        actionBar.setTitle(TasksListFragment.TITLE);
                        break;
                    /*case R.id.action_stats:
                        findViewById(R.id.layoutProjects).setVisibility(View.GONE);
                        findViewById(R.id.layoutTasks).setVisibility(View.GONE);
                        findViewById(R.id.layoutStats).setVisibility(View.VISIBLE);
                        toolbar.getMenu().clear();
                        break;*/
                }
                return true;
            }
        });
    }

    private void replaceDetailFragment(long id) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        ProjectDetailFragment detailFragment = (ProjectDetailFragment) fragmentManager.findFragmentByTag(ProjectDetailFragment.TAG);
        if (detailFragment != null) {
            transaction.remove(detailFragment);
        }

        detailFragment = new ProjectDetailFragment();
        detailFragment.setProject((int) id);

        transaction.replace(R.id.project_detail_fragment_container, detailFragment, ProjectDetailFragment.TAG);
        //transaction.addToBackStack(ProjectDetailFragment.TAG);

        transaction.commit();
    }

    @Override
    public void itemClicked(long id, Class T) {
        if (this.detailFragmentContainer != null) {
            replaceDetailFragment(id);
        }
        else {
            Intent intent = new Intent(this, T);
            intent.putExtra(DetailProjectActivity.EXTRA_FIELD_ID, (int) id);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.projects_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.new_task) {
            Intent intent = new Intent(this, NewTaskActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.new_project) {
            Intent intent = new Intent(this, NewProjectActivity.class);
            startActivity(intent);
        }
        return true;
    }


    public Toolbar getToolbar() {
        return toolbar;
    }

    public static TrackitDatabaseHelper getTrackitDatabaseHelper() {
        return trackitDatabaseHelper;
    }
}
