package icu.lifelesshub.trackit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import icu.lifelesshub.trackit.activity.MainActivity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    private boolean checkLoginPassword(String login, String password) {
        return true; // temporary
    }
    public void onClickLogin(View view) {
        EditText login = (EditText) findViewById(R.id.auth_login);
        EditText password = (EditText) findViewById(R.id.auth_password);
        if (checkLoginPassword(login.getText().toString(), password.getText().toString())) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
