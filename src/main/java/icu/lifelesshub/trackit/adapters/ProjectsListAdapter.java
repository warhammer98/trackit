package icu.lifelesshub.trackit.adapters;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.entities.Project;

public class ProjectsListAdapter extends ArrayAdapter<Project> {
    ListView listView;
    Context context;
    LayoutInflater layoutInflater;
    private int resourceLayout;
    ArrayList<Project> projects;

    public Project getProjectByIndex(int idx) {
        return projects.get(idx);
    }
    public ProjectsListAdapter(Context context, int resource, ArrayList<Project> projects) {
        super(context, resource, projects);
        this.context = context;
        this.projects = projects;
        this.resourceLayout = resource;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.listView == null) {
            this.listView = (ListView) parent;
            listView.setDivider(null); // hide divider
            listView.setDividerHeight(0);
            listView.setSelector(new StateListDrawable()); // disable ripple effect on items, leaves custom ripple
        }


        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(context);
            v = vi.inflate(resourceLayout, null);
        }

        Project p = getItem(position);

        if (p != null) {
            TextView projectNameField = (TextView) v.findViewById(R.id.project_name);
            if (projectNameField != null) {
                projectNameField.setText(p.getName());
            }
            TextView tasksCountField = (TextView) v.findViewById(R.id.tasks_count);
            if (tasksCountField != null) {
                tasksCountField.setText(p.getTasksCount() + " " + (p.getTasksCount() == 1? "task" : "tasks"));
            }
        }

        return v;
    }

}
