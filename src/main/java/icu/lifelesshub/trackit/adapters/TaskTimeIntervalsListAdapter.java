package icu.lifelesshub.trackit.adapters;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.widget.PopupMenu;

import java.util.ArrayList;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.activity.MainActivity;
import icu.lifelesshub.trackit.activity.task.DetailTaskActivity;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.entities.TaskTimeInterval;


public class TaskTimeIntervalsListAdapter extends ArrayAdapter<TaskTimeInterval> {
    ListView listView;
    Context context;
    LayoutInflater layoutInflater;
    private int resourceLayout;
    ArrayList<TaskTimeInterval> intervals;
    private int taskId;
    private Runnable timer;
    private Handler handler;
    public TaskTimeInterval getIntervalByIndex(int idx) {
        return intervals.get(idx);
    }
    public TaskTimeIntervalsListAdapter(Context context, int resource, ArrayList<TaskTimeInterval> intervals) {
        super(context, resource, intervals);
        this.context = context;
        this.intervals = intervals;
        this.resourceLayout = resource;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.listView == null) {

            this.listView = (ListView) parent;
            listView.setDivider(null); // hide divider
            listView.setDividerHeight(0);
            listView.setSelector(new StateListDrawable()); // disable ripple effect on items, leaves custom ripple
            listView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,getSupposedHeight()));
        }

        View v = convertView;


        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(context);
            v = vi.inflate(resourceLayout, null);
        }
        TaskTimeInterval interval = getItem(position);
        Task task = Task.getTaskById(taskId);
        if (interval != null) {
            TextView taskNameField = (TextView) v.findViewById(R.id.task_name);
            if (taskNameField != null) {
                taskNameField.setText(task.getName() + " (" + interval.getId() + ")");
            }
            updateTimeString(v, interval);
            if (interval.getStopTime() == 0 ) {
                startTimer(v,interval);
            }
        }

        return v;
    }
    public void updateTimeString(View v, TaskTimeInterval interval) {

        TextView intervalTimeField = (TextView) v.findViewById(R.id.interval_time);
        if (intervalTimeField != null) {
            intervalTimeField.setText(interval.getTimeString());
        }
    }

    private void startTimer(final View view, final TaskTimeInterval interval) {

        handler = new Handler();
        timer = new Runnable() {
            @Override
            public void run() {
                String name = ( (TextView)view.findViewById(R.id.task_name)).getText().toString();
                updateTimeString(view, interval);
                if (interval.getStopTime() == 0 ) {
                    handler.postDelayed(this, 1000);
                }
            }
        };
        handler.post(timer);
    }


    public int getSupposedHeight() {
        TaskTimeIntervalsListAdapter adapter = (TaskTimeIntervalsListAdapter) listView.getAdapter();

        int items = intervals.size();
        int grossElementHeight = 0;
        for (int i = 0; i < items; i++) {
            View childView = adapter.getView(i, null, listView);
            childView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            grossElementHeight += childView.getMeasuredHeight();
        }
        return grossElementHeight;
    }


    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
}
