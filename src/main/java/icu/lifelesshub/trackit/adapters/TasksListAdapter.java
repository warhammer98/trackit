package icu.lifelesshub.trackit.adapters;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

import icu.lifelesshub.trackit.R;
import icu.lifelesshub.trackit.entities.Task;
import icu.lifelesshub.trackit.other.UIHelper;


public class TasksListAdapter extends ArrayAdapter<Task> {
    ListView listView;
    Context context;
    LayoutInflater layoutInflater;
    private int resourceLayout;
    private boolean isScrollable = true;
    ArrayList<Task> tasks;
    private Runnable timer;
    private Handler handler;
    public Task getTaskByIndex(int idx) {
        return tasks.get(idx);
    }
    public TasksListAdapter(Context context, int resource, ArrayList<Task> tasks) {
        super(context, resource, tasks);
        this.context = context;
        this.tasks = tasks;
        this.resourceLayout = resource;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setScrollable(boolean scrollable) {
        isScrollable = scrollable;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.listView == null) {

            this.listView = (ListView) parent;
            listView.setDivider(null); // hide divider
            listView.setDividerHeight(0);
            listView.setSelector(new StateListDrawable()); // disable ripple effect on items, leaves custom ripple
            if (!isScrollable) { // disable scroll
                listView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,getSupposedHeight()));
            }
        }

        View v = convertView;


        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(context);
            v = vi.inflate(resourceLayout, null);
        }
        if (!isScrollable) {
            View inner = v.findViewById(R.id.list_item_inner);
            inner.setElevation(3 * context.getResources().getDisplayMetrics().density);
        }
        Task t = getItem(position);

        if (t != null) {
            TextView taskNameField = (TextView) v.findViewById(R.id.task_name);
            if (taskNameField != null) {
                taskNameField.setText(t.getName());
            }

            updateTimeString(v,t);
            if (t.getStartTime() > 0 ) {
                startTimer(v,t);
            }
        }

        return v;
    }
    public void updateTimeString(View v, Task task) {

        TextView taskTimeField = (TextView) v.findViewById(R.id.task_time);
        if (taskTimeField != null) {
            taskTimeField.setText(task.getTimeString());
        }
    }
    private void startTimer(final View view, final Task task) {

        handler = new Handler();
        timer = new Runnable() {
            @Override
            public void run() {
                String name = ( (TextView)view.findViewById(R.id.task_name)).getText().toString();
                if (!name.equals(task.getName())) {
                    return;
                }
                updateTimeString(view, task);
                if (task.getStartTime() > 0 ) {
                    handler.postDelayed(this, 1000);
                }
            }
        };
        handler.post(timer);
    }



    public int getSupposedHeight() {
        TasksListAdapter adapter = (TasksListAdapter) listView.getAdapter();

        int items = tasks.size();
        int grossElementHeight = 0;
        for (int i = 0; i < items; i++) {
            View childView = adapter.getView(i, null, listView);
            childView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            grossElementHeight += childView.getMeasuredHeight();
        }
        return grossElementHeight;
    }
}
